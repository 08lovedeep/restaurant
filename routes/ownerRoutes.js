const { Router } = require("express");
const resController = require("../controllers/resControllers");
const router = Router();

// All routes are valid only when user will be authenticated which we do via middleware used in app.js

router.patch("/owner/:id/edit", resController.editOwner);
router.post("/owner/:id/restaurant/create", resController.newRestaurants);
router.patch("/owner/restaurant/:id/edit", resController.editRestaurants);
router.delete("/owner/restaurant/:id/delete", resController.deleteRestaurants);

module.exports = router;
