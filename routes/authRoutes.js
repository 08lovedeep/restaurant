const { Router } = require("express");
const authController = require("../controllers/authControllers");
const router = Router();
const { requireAuth } = require("../Middleware/authMiddleware");

router.get("/signup", requireAuth, authController.signup_get);
router.post("/signup", requireAuth, authController.signup_post);
router.get("/login", requireAuth, authController.login_get);
router.post("/login", requireAuth, authController.login_post);
router.get("/logout", requireAuth, authController.logout_get);

module.exports = router;
