# Installation Guide

1. After getting code go to file and do npm install to install necessary dependencies
2. Run nodemon seeds.js file to get a temporary database (though not neccessary if you want to create your own)
3. Run nodemon app.js to start the server

#### All Routes will be Protected and can't be viewed , you have to first signup to run or test all apis.

# Guide for running mongo db on pc

1. Open terminal and write mongod to run mongo server
2. Open other terminal and write mongo to use database
