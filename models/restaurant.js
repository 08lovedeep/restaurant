const mongoose = require("mongoose");
const User = require("../models/user");
const Schema = mongoose.Schema;

const resSchema = new mongoose.Schema({
  owner: {
    type: Schema.Types.ObjectId,
    ref: "User",
  },
  name: {
    type: String,
    required: true,
  },
  city: {
    type: String,
    required: true,
  },
  diningPrice: {
    type: Number,
    required: true,
  },
  active: {
    type: Boolean,
    required: true,
  },
});

const Restaurant = mongoose.model("restaurant", resSchema);

module.exports = Restaurant;
