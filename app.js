const express = require("express");
const app = express();
const mongoose = require("mongoose");
const authRoutes = require("./routes/authRoutes");
const ownerRoutes = require("./routes/ownerRoutes");
const userRoutes = require("./routes/userRoutes");
const adminRoutes = require("./routes/adminRoutes");
const cookieParser = require("cookie-parser");
const { requireAuth, checkUser } = require("./Middleware/authMiddleware");

app.use(express.static("public"));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cookieParser());
app.set("view engine", "ejs");

mongoose
  .connect("mongodb://localhost:27017/restaurantbackend", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Mongo Connection Open");
  })
  .catch(err => {
    console.log("Oh no mongo error!!!");
  });

app.use(authRoutes);
app.get("*", checkUser, requireAuth);
app.use(ownerRoutes);
app.use(userRoutes);
app.use(adminRoutes);
app.listen(3000, () => {
  console.log("App Listening On Port 3000");
});
